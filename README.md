HandBrake Builder
-----------------

This image uses Fedora as the base, then adds development tools and compilers 
for cross-compiling HandBrake (mingw-w64) for Windows.

This is not a generalized mingw-w64 image, because it is created using the 
HandBrake project's [script](https://github.com/HandBrake/HandBrake/blob/master/scripts/mingw-w64-build), 
which uses a specific set of library versions.

The public location for this repository's source should be 
[https://gitlab.com/frederickding/handbrake-builder](https://gitlab.com/frederickding/handbrake-builder), 
where the Docker image can be pulled:

```
docker pull registry.gitlab.com/frederickding/handbrake-builder:master
```

The mingw-w64 executables will be located in the directory that `$TOOLCHAIN_PATH` 
points to, which will also be prepended to the `$PATH`.
